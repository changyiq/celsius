package sheridan;
/*
 * @author Yiqian Chang 991554674
 */
public class Celsius {

	public static int fromFahrenheit(int f) {
		
		int celsius = (int) Math.round(( 5 *(f - 32.0)) / 9.0);
		
		return celsius;
	}
}
