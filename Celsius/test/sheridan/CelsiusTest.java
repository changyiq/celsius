package sheridan;

import static org.junit.Assert.*;

import org.junit.Test;

import com.sun.net.httpserver.Authenticator.Result;

/*
 * @author Yiqian Chang 991554674
 */

public class CelsiusTest {

	@Test
	public void testFromFahrenheitRegular() {
		
		int CelsiusResult = Celsius.fromFahrenheit(200); 
		assertTrue("Invalid Fahrenheit", CelsiusResult == 93);
	}
		
	@Test
	public void testFromFahrenheitExceptional() {
		int CelsiusResult = Celsius.fromFahrenheit(200); 
		assertFalse("Invalid Fahrenheit", CelsiusResult == 90);
	}
	
	@Test
	public void testFromFahrenheitboundaryIn() {
		int CelsiusResult = Celsius.fromFahrenheit(101); 
		assertTrue("Invalid Fahrenheit", CelsiusResult == 38);
	}

	@Test
	public void testFromFahrenheitboundaryOut() {
		int CelsiusResult = Celsius.fromFahrenheit(100); 
		assertFalse("Invalid Fahrenheit", CelsiusResult == 37);
	}

}
